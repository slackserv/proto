package main

import (
	"testing"
	"encoding/json"
)

func TestIndex (t *testing.T) {
	r := NewRunlevel(`2`)
	if r.Index() != 3 {
		t.Error("Index() failed!")
	}
}

func TestIsSet (t *testing.T) {
	rs := Runlevels{false, true, false, false, false, false, false, false, true}
	if !rs.IsSet(`0`) {
		t.Error("IsSet(0) failed")
	}
	if rs.IsSet(`1`) {
		t.Error("IsSet(1) failed")
	}
	if !rs.IsSet(`S`) {
		t.Error("IsSet(S) failed")
	}
}

func TestSet (t *testing.T) {
	var rs Runlevels
	if rs.IsSet(`5`) {
		t.Error("static initialzation failed")
	}
	if err := rs.Set(`5`); err != nil {
		t.Error(err.Error())
	}
	if !rs.IsSet(`5`) {
		t.Error("Set(5) failed")
	}
}

func TestString (t *testing.T) {
	s := Runlevels{false, true, false, false, false, false, false, true, true}.String()
	if s != `0,6,S` {
		t.Error("String failed")
	}
}

func TestUnmarshalJSON(t *testing.T) {
	var jsonBlob = []byte(`["1","0","S","6"]`)
	var rs Runlevels
	if err := json.Unmarshal(jsonBlob, &rs); err != nil {
		t.Error(err.Error())
	}
	if rs.String() != `0,1,6,S` {
		t.Error("unmarshal failed")
	}
}

func TestMarshalJSON(t *testing.T) {
	rs := Runlevels{false, true, false, false, false, false, false, true, true}
	bytes, err := json.Marshal(rs)
	if err != nil {
		t.Error("marshal failed")
	}
	if string(bytes) != `"0,6,S"` {
		t.Error("marshal produced wrong result " + string(bytes));
	}
}

func TestIterator(t *testing.T) {
	rs := Runlevels{false, true, false, false, false, false, false, true, true}
	expect := `06S`
	got := ``
	next := rs.Iterator()
	r := next()
	for r.IsValid() {
		got += r.Letter()
		r = next()
	}
	if got != expect {
		t.Error("Iterator failed")
	}
}
