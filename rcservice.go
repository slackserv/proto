package main

// RCService represents a link to a System V service file from a
// rcN.d directory.
type RCService struct {
	SysVService        // The service itself.
	priority int       // Relative priority number.
}

func (srv *RCService) Copy(s Service) {
	srv.SysVService.Copy(s)
	srv.priority = s.Priority()
}

func (srv *RCService) Priority() int {
	return srv.priority
}
func (srv *RCService) SetPriority(n int) {
	srv.priority = n
}

func NewRCService(src Service) Service {
	var dst RCService
	dst.Copy(src)
	dst.priority = -1
	return &dst
}
