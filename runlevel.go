package main

import (
	"errors"
	"strings"
	"encoding/json"
)

var runlevelLetter = [...]string{`?`, `0`, `1`, `2`, `3`, `4`, `5`, `6`, `S`}
const NumLevels = len(runlevelLetter)

// Runlevel represents a single runlevel.
type Runlevel byte

// NewRunlevel returns a runlevel corresponding to the letter s
func NewRunlevel(s string) Runlevel {
	for i := 1; i < NumLevels; i++ {
		if runlevelLetter[i] == s {
			return Runlevel(i)
		}
	}
	return Runlevel(0)
}

// Index returns internal representation of the runlevel
func (r Runlevel) Index() int {
	return int(r)
}

// Letter returns runlevel letter (external representation)
func (r Runlevel) Letter() string {
	return runlevelLetter[r]
}

// IsValid returns  true if the runlevel is valid
func (r Runlevel) IsValid() bool {
	return r > 0 && int(r) < NumLevels
}

// String returns a string representation of the runlevel (its letter,
// actually)
func (r Runlevel) String() string {
	return r.Letter()
}

// Runlevels is a set of runlevels
type Runlevels [NumLevels]bool

// The Set function sets the runlevel indicated by the letter let.
// On success, returns nil. Returns error, if no runlevel corresponds
// to the letter let.
func (rs *Runlevels) Set(let string) error {
	r := NewRunlevel(let)
	if r.IsValid() {
		rs[r.Index()] = true
		return nil
	}
	return errors.New(let + `: unknown runlevel`)
}

// Sets all runlevels in the rs.
func (rs *Runlevels) SetAll() {
	for i, _ := range rs[1:] {
		rs[i] = true
	}
}

// IsSet returns true if the runlevel let is set in rs.
func (rs Runlevels) IsSet(let string) bool {
	r := NewRunlevel(let)
	if r.IsValid() {
		return rs[r.Index()]
	}
	return false
}

// Returns the count of runlevels set in rs.
func (rs Runlevels) Count() int {
	count := 0
	for _, isset := range rs[1:] {
		if isset {
			count++
		}
	}
	return count
}

// Logical OR between two runlevel sets
func (rs *Runlevels) Or(other Runlevels) {
	for i, isset := range other {
		rs[i] = rs[i] || isset
	}
}

// Logical AND between two runlevel sets
func (rs *Runlevels) And(other Runlevels) {
	for i, isset := range other {
		rs[i] = rs[i] && isset
	}
}

type RunlevelIterator func () Runlevel

func (rs Runlevels) Iterator() RunlevelIterator {
	i := 0
	return func() Runlevel {
		i++
		for true {
			if (i >= NumLevels) {
				return Runlevel(0)
			}
			if rs[i] {
				break
			}
			i++
		}
		return Runlevel(i)
	}
}

// String returns a string representation of the runlevel set
func (rs Runlevels) String() string {
	var s []string
	for i, isset := range rs {
		if isset {
			s = append(s, Runlevel(i).String())
		}
	}
	return strings.Join(s, `,`)
}

func (rsret *Runlevels) UnmarshalJSON(b []byte) error {
	var s []string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	var rs Runlevels
	for _, let := range s {
		if err := rs.Set(let); err != nil {
			return err
		}
	}
	*rsret = rs
	return nil
}

func (rs Runlevels) MarshalJSON() ([]byte, error) {
	return json.Marshal(rs.String())
}
