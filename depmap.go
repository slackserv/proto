package main

import (
	"errors"
	"strconv"
)

// The dependency map structure
type Depmap struct {
	dist [][]int    // Distance between two vertices
	next [][]int    // Next vertex on the shortest path from i to j
	dim int         // Number of vertices
	closed bool     // Whether transitive closure was computed
}

// Create new dependency map
func NewDepmap(n int) *Depmap {
	v := Depmap{dim: n, closed: false}
	v.dist = make([][]int, n)
	v.next = make([][]int, n)
	for i := 0; i < n; i++ {
		v.dist[i] = make([]int, n)
		v.next[i] = make([]int, n)
		for j := 0; j < n; j++ {
			v.dist[i][j] = n + 1 // infinity
			v.next[i][j] = -1
		}
	}
	return &v
}

// Create exact copy of the map
func (dm *Depmap) Copy() *Depmap {
	v := Depmap{dim: dm.dim, closed: dm.closed}
	v.dist = make([][]int, dm.dim)
	v.next = make([][]int, dm.dim)
	for i := 0; i < v.dim; i++ {
		v.dist[i] = make([]int, dm.dim)
		copy(v.dist, dm.dist)
		v.next[i] = make([]int, dm.dim)
		copy(v.next, dm.next)
	}
	return &v
}

// Return the dimension of the map
func (dm *Depmap) Dim() int {
	return dm.dim
}

// Return the infinity distance value for this map
func (dm *Depmap) Inf() int {
	return dm.Dim() + 1
}

// Compute transitive closure with the shortest path reconstruction
func (dm *Depmap) TC() {
	if dm.closed {
		// Nothing changed since the closure had been computed
		return
	}

	// Initialize next vertex map and reset all distances between
	// adjacent vertices to 1.
	for i := 0; i < dm.dim; i++ {
		for j := 0; j < dm.dim; j++ {
			if dm.IsSet(i,j) {
				dm.dist[i][j] = 1
				dm.next[i][j] = j
			} else {
				dm.next[i][j] = -1
			}
		}
	}

	// Modified Floyd-Warshall algorithm
	for k := 0; k < dm.dim; k++ {
		for i := 0; i < dm.dim; i++ {
			for j := 0; j < dm.dim; j++ {
				t := dm.dist[i][k] + dm.dist[k][j]
				if dm.dist[i][j] > t {
					dm.dist[i][j] = t
					dm.next[i][j] = dm.next[i][k]
				}
			}
		}
	}

	// Mark completion
	dm.closed = true
}

// Set distance between vertices i and j to 1.
func (dm *Depmap) Set(i, j int) {
	dm.dist[i][j] = 1
	dm.closed = false
}

// Set distance between vertices i and j to infinity.
func (dm *Depmap) Unset(i int, j int) {
	dm.dist[i][j] = dm.Inf()
	dm.closed = false
}

// Return the (minimal) distance between the two vertices.
func (dm *Depmap) Dist(i, j int) int {
	return dm.dist[i][j]
}

// Return true if the two vertices are connected.
func (dm *Depmap) IsSet(i, j int) bool {
	return dm.Dist(i,j) != dm.Inf()
}

// Iterator over the dependency map.  Returns the index of the next
// vertex and true, if such vertex exists, or false otherwise.
type DepmapIterator func () (int, bool)

// Return the iterator over each slot in the row i that is set,
// i.e. over each vertex that is reachable from i
// i.e. over each vertex on which vertex i depends
func (dm *Depmap) TraverseRow(i int) (DepmapIterator, error) {
	if i < 0 || i > dm.dim {
		return nil, errors.New(strconv.Itoa(i) + ": out of bounds");
	}
	j := 0
	return func () (int, bool) {
		for j < dm.dim {
			k := j
			j++
			if dm.IsSet(i, k) {
				return k, true
			}
		}
		return -1, false
	}, nil
}

// Return the iterator over each slot in the column i that is set,
// i.e. over each vertex from which i is reachable
// i.e. over each vertex that depends on vertex i
func (dm *Depmap) TraverseColumn(i int) (DepmapIterator, error) {
	if i < 0 || i > dm.dim {
		return nil, errors.New(strconv.Itoa(i) + ": out of bounds");
	}
	j := 0
	return func () (int, bool) {
		for j < dm.dim {
			k := j
			j++
			if (dm.IsSet(k, i)) {
				return k, true
			}
		}
		return -1, false
	}, nil
}

// Return an iterator over each vertex is connected to the same vertices
// as the vertex n,
// i.e. over each vertex that has the same dependencies as n
func (dm *Depmap) TraverseMatchingRows (n int) (DepmapIterator, error) {
	if n < 0 || n > dm.dim {
		return nil, errors.New(strconv.Itoa(n) + ": out of bounds");
	}
	i := 0
	return func() (int, bool) {
	    nextrow:
		for i < dm.dim {
			k := i
			i++
			if k == n {
				continue
			}
			for j := 0; j < dm.dim; j++ {
				if dm.IsSet(k,j) != dm.IsSet(n,j) {
					continue nextrow
				}
			}
			return k, true
		}
		return -1, false
	}, nil
}

// Returns the path from vertex i to j as array of vertices to traverse.
func (dm *Depmap) Path(i, j int) (path []int) {
	if dm.closed && dm.next[i][j] > 0 {
		path = append(path, i)
		for i != j {
			i = dm.next[i][j]
			path = append(path, i)
		}
	}
	return
}

// If the vertex i is a part of a cyclic path, return the path adjusted
// so that it starts at the vertex with the lowest number among the vertices
// traversed by that path.
func (dm *Depmap) NormalizedCyclePath(i int) (path []int) {
	if dm.closed && dm.IsSet(i,i) {
		path = dm.Path(dm.next[i][i], i)
		minval := dm.Inf()
		minpos := 0
		for i, n := range path {
			if n < minval {
				minval = n
				minpos = i
			}
		}
		if minpos != 0 {
			path = append(path[minpos:], path[0:minpos]...)
		}
		path = append(path[len(path)-1:], path...)
	}
	return
}

// Return true if the path is already in the array of paths p.
func hasPath(p [][]int, path []int) bool {
	pathlen := len(path)
    retry:
	for _, track := range p {
		if len(track) == pathlen {
			for i, v := range path {
				if track[i] != v {
					continue retry
				}
			}
			return true
		}
	}
	return false
}

// Return all cyclic paths in the dependency map.
func (dm *Depmap) FindCycles() ([][]int) {
	var p [][]int
	dm.TC()
	for i := 0; i < dm.dim; i++ {
		path := dm.NormalizedCyclePath(i)
		if len(path) > 0 && ! hasPath(p, path) {
			p = append(p, path)
		}
	}
	return p
}
