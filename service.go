package main

import "os"

// Abstract Service type
type Service interface {
	Copy(Service)
	FileName() string
	BaseName() string
	SetFileName(string)
	SetBaseName(string)
	FileInfo() os.FileInfo
	SetFileInfo(os.FileInfo)
	SameFile(os.FileInfo) bool
	SetPriority(int)
	Priority() int
	Provided() []string
	Provides(string) bool
	RequiredStart() []string
	RequiredStop() []string
	ShouldStart() []string
	ShouldStop() []string
	DefaultStart() Runlevels
	DefaultStop() Runlevels
	ShortDescr() string
	Descr() string
	Enabled() (bool, string)
}

type ServiceIterator func() Service

// Base service structure - a common part for all services
type ServiceBase struct {
	// Base service name is the name of the service without eventual
	// start indicator and ordinal sequence number.  IOW, it is the
	// base name of the corresponding service file in init.d
	baseName string
	fileInfo os.FileInfo   // Information about the base service file.
	fileName string        // Name of the service file.
	shortDescr string      // A one-line description of the service.
	descr string           // Multi-linde desctiption of the service.
	provided []string      // Array of provided keywords.
}

// Copy service name and file info from s,
func (srv *ServiceBase) Copy(s Service) {
	srv.baseName = s.BaseName()
	srv.fileInfo = s.FileInfo()
	srv.fileName = s.FileName()
}

// Return the file name of the service.
func (srv *ServiceBase) FileName() string {
	return srv.fileName
}

// Set the service file name.
func (srv *ServiceBase) SetFileName(s string) {
	srv.fileName = s
}

// Return the service base name.
func (srv *ServiceBase) BaseName() string {
	return srv.baseName
}

// Set the service base name,
func (srv *ServiceBase) SetBaseName(s string) {
	srv.baseName = s
}

// Return the service file information.
func (srv *ServiceBase) FileInfo() os.FileInfo {
	return srv.fileInfo
}

// Set the service file information.
func (srv *ServiceBase) SetFileInfo(info os.FileInfo) {
	srv.fileInfo = info
}

// Return true if the base file for the service srv is the same as the file
// described by inf.
func (srv *ServiceBase) SameFile(inf os.FileInfo) bool {
	return os.SameFile(srv.fileInfo, inf)
}

// Return the short description of the service.
func (srv *ServiceBase)	ShortDescr() string {
	return srv.shortDescr
}

// Set the short description of the service.
func (srv *ServiceBase)	Descr() string {
	return srv.descr
}

// Return array of symbols provided by the service.
func (srv *ServiceBase)	Provided() []string {
	return srv.provided
}

// Return true if the service srv provides the symbol sym.
func (srv *ServiceBase)	Provides(sym string) bool {
	for _, s := range srv.Provided() {
		if s == sym {
			return true
		}
	}
	return false
}

// Abstract methods
func (srv *ServiceBase) Priority() int {
	return 0
}
func (srv *ServiceBase) SetPriority(n int) {
	panic("method not implemented")
}
func (srv *ServiceBase)	RequiredStart() []string {
	return []string{}
}
func (srv *ServiceBase)	RequiredStop() []string {
	return []string{}
}
func (srv *ServiceBase)	ShouldStart() []string {
	return []string{}
}
func (srv *ServiceBase)	ShouldStop() []string {
	return []string{}
}
func (srv *ServiceBase)	DefaultStart() Runlevels {
	return Runlevels{}
}
func (srv *ServiceBase)	DefaultStop() Runlevels {
	return Runlevels{}
}
func (srv *ServiceBase) Enabled() (bool, string) {
	return true, "on"
}
