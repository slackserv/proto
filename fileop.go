package main

import (
	"strings"
	"os"
	"path/filepath"
	"log"
)

// The Symlink structure contains a single instruction for creating a
// symbolic link to the service file in init.d.
type Symlink struct {
	dir string       // Full directory name of the rcN.d directory
	src string       // Source argument name
	dst string       // Name of the symlink to create
}

// Create a symlink requested by s.
func (s Symlink) Commit() error {
	// Find longest common directory prefix
	if opts.Debug {
		log.Printf("DEBUG: dir=%s src=%s dst=%s\n", s.dir, s.src, s.dst)
	}
	a := strings.Split(s.dst, `/`)
	if (a[0] == ``) {
		a[0] = `/`
	}
	b := strings.Split(s.src, `/`)
	if (b[0] == ``) {
		b[0] = `/`
	}
	n := len(a)
	if len(b) < n {
		n = len(b)
	}
	n--

	if n > 0 {
		i := 0
		for i = 0; i < n; i++ {
			if a[i] != b[i] {
				break
			}
		}
		if i > 0 {
			s.dir = filepath.Join(a[:i+1]...);
			s.dst = filepath.Join(a[i+1:]...)
			src := b[i:]
			src = append([]string{`..`}, src...)
			i--
			s.src = filepath.Join(src...)
		}
	} else {
		s.dir = `/`
	}

	if opts.Debug {
		log.Printf("DEBUG: dir=%s src=%s dst=%s\n", s.dir, s.src, s.dst)
	}

	// Create the link, unless in dry-run mode:
	if ! opts.DryRun {
		cwd, err := os.Getwd()
		if err != nil {
			return err
		}

		if err := os.Chdir(s.dir); err != nil {
			return err
		}

		if err := os.Symlink(s.src, s.dst); err != nil {
			return err
		}

		if err := os.Chdir(cwd); err != nil {
			return err
		}
	}

	return nil
}
// ----------------------------
// The Unlink structure contains a single instruction for unlinking a
// symbolic link
type Unlink struct {
	name string
}

func (s Unlink) Commit() error {
	if opts.Debug {
		log.Printf("DEBUG: removing %s\n", s.name)
	}
	if ! opts.DryRun {
		return os.Remove(s.name)
	}
	return nil
}

func NewUnlink(name string) *Unlink {
	st, err := os.Lstat(name)
	if err != nil {
		log.Printf("%s: can't stat: %s\n", name, err.Error())
		os.Exit(1)
	}
	if ((st.Mode() & os.ModeSymlink) == 0) {
		log.Printf("%s: can't schedule for unlink: not a symbolic link\n",
			      name)
		os.Exit(1)
	}
	return &Unlink{name: name}
}

// ----------------------------
// File operation interface
// ----------------------------
type FileOp interface {
	Commit() error
}

type FileOpList struct {
	ops []FileOp
}

// Commit all operations in oplist.  Return first encountered error or nil
// on success.
func (oplist *FileOpList) Commit() error {
	for _, op := range oplist.ops {
		if err := op.Commit(); err != nil {
			return err
		}
	}
	return nil
}

// Append one or more operations to the FileOpList.
func (oplist *FileOpList) Append(op ... FileOp) {
	oplist.ops = append(oplist.ops, op...)
}

// Concatenate two FileOpLists.
func (oplist *FileOpList) Concat(opl *FileOpList) {
	oplist.Append(opl.ops...)
}
