package main

import (
	"os"
	"path/filepath"
)

type BuiltinService struct {
	ServiceBase
	rcfile string
	priority int
}

// LSB 3.1 virtual facility names
// http://refspecs.linuxfoundation.org/LSB_3.1.0/LSB-Core-generic/LSB-Core-generic/facilname.html
// FIXME: Support also $all: https://wiki.debian.org/LSBInitScripts
var builtinServices = []BuiltinService{
	// Always true:
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$local_fs`,
				provided: []string{`$local_fs`},
				shortDescr: `all local file systems are mounted` },
			},
	// True, unless single-user
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$network`,
				provided: []string{`$network`},
				shortDescr: `basic networking support is available` },
			},
	// True if /etc/rc.d/rc.named has executable bit on
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$named`,
				provided: []string{`$named`},
				shortDescr: `IP name-to-address translation is available` },
			 rcfile: `rc.named` },
	// True if /etc/rc.d/rc.rpc has executable bit on
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$portmap`,
				provided: []string{`$portmap`, `$rpcbind`},
				shortDescr: `SunRPC/ONCRPC portmapping service is available` },
			 rcfile: `rc.rpc` },
	// True (unless single-user)
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$remote_fs`,
				provided: []string{`$remote_fs`},
				shortDescr: `all remote file systems are available` },
			 },
	// True if /etc/rc.d/rc.syslog has executable bit on
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$syslog`,
				provided: []string{`$syslog`},
				shortDescr: `system logger is operational` },
			rcfile: `rc.syslog` },
	// Always true (or: True if /etc/rc.d/rc.ntpd has executable bit on)
	BuiltinService{ ServiceBase: ServiceBase{
				baseName: `$time`,
				provided: []string{`$time`},
				shortDescr: `the system time is set` },
			rcfile: `rc.ntpd` } }

func (srv *BuiltinService) Enabled() (bool, string) {
	if srv.rcfile != `` {
		name := filepath.Join(opts.RCDirectory, srv.rcfile)
		info, err := os.Stat(name);
		if err != nil {
			return false, err.Error()
		}
		if (info.Mode() & 0111) == 0 {
			return false, "run chmod +x " + name + ", to enable it";
		}
	}
	return true, `on`
}

func (srv *BuiltinService) Priority() int {
	return srv.priority
}

func (srv *BuiltinService) SetPriority(n int) {
	srv.priority = n
}

func (srv *BuiltinService) Copy(s Service) {
	srv.ServiceBase.Copy(s)
	srv.priority = s.Priority()
}

func BuiltinServiceIterator(start bool) ServiceIterator {
	prio := NewPriority(start)
	i := 0
	return func () Service {
		if i == len(builtinServices) {
			return nil
		}
		srv := builtinServices[i]
		srv.SetPriority(prio.Value())
		i++
		return &srv
	}
}

func IsBuiltinServiceName(s string) bool {
	for _, srv := range builtinServices {
		if srv.BaseName() == s {
			return true
		}
	}
	return false
}
