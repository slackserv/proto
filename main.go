package main

import (
	"github.com/pborman/getopt/v2"
	"fmt"
	"os"
	"log"
	"path/filepath"
	"strings"
)

var opts struct {
	List bool
	Add  bool
	Del  bool
	Recursive bool
	Defaults bool
	Level []string
	RCDirectory string
	DryRun bool
	Debug bool
	Wide bool
}

var InitDirectory string

func main() {
	log.SetPrefix(filepath.Base(os.Args[0]) + ": ")
	log.SetFlags(log.Lmsgprefix)
	getopt.FlagLong(&opts.List, "list", 'l', "List available services")
	getopt.FlagLong(&opts.Add, "add", 'a', "Add services")
	getopt.FlagLong(&opts.Del, "del", 'd', "Delete services")
	getopt.FlagLong(&opts.Recursive, "recursive", 'r', "Recursively process dependencies")
	getopt.FlagLong(&opts.Defaults, "defaults", 'D', "Restore defaults")
	getopt.FlagLong(&opts.Level, "level", 'L', "Apply to these runlevels")
	opts.RCDirectory = "/etc/rc.d"
	getopt.FlagLong(&opts.RCDirectory, "directory", 'C', "Location of the rc.d directory")//FIXME: init
	getopt.FlagLong(&opts.DryRun, "dry-run", 'n', "Do nothing; print what would have been done")
	getopt.FlagLong(&opts.Debug, "debug", 'x', "Print debug info")
	getopt.FlagLong(&opts.Wide, "wide", 'w', "Wide output")
	help := false
	getopt.FlagLong(&help, "help", 'h', "Show this help output")

	getopt.Parse()

	if help {
		getopt.PrintUsage(os.Stdout)
		os.Exit(0)
	}

	args := getopt.Args()

	if opts.DryRun {
		opts.Debug = true
	}
	InitDirectory = filepath.Join(opts.RCDirectory, `init.d`)

	if opts.Add {
		if opts.List || opts.Del {
			log.Fatal("conflicting options")
		}
		Add(NewArgList(args))
		os.Exit(0)
	}
	if opts.Del {
		if opts.List || opts.Add {
			log.Fatal("conflicting options")
		}
		Del(NewArgList(args))
		os.Exit(0)
	}

	//Default: list
	List(NewArgList(args))
}

type ArgList struct {
	args []string
}

func NewArgList(args []string) *ArgList {
	var a ArgList
	a.args = args
	return &a
}

func (a *ArgList) GetNameSelector() NameSelector {
	if len(a.args) > 0 {
		return func (n string) bool {
			for i, s := range a.args {
				if s == n {
					a.args = append(a.args[:i], a.args[i+1:]...)
					return true
				}
			}
			return false
		}
	} else {
		return func (n string) bool {
			return true
		}
	}
}

func (a *ArgList) Len() int {
	return len(a.args)
}

func (a *ArgList) Consumed() bool {
	return a.Len() == 0
}

func (a *ArgList) Report() {
	if !a.Consumed() {
		for _, name := range a.args {
			log.Println(name + ": unknown service")
		}
		os.Exit(1)
	}
}
//
func GetRunlevels() Runlevels {
	var rs Runlevels
	if len(opts.Level) > 0  {
		for _, let := range opts.Level {
			if err := rs.Set(let); err != nil {
				log.Fatal(err)
			}
		}
	} else {
		rs.SetAll()
	}
	return rs
}

// -------------------------------------
// 1. Get service listing
// -------------------------------------

// A row in the listing
type ListingRow struct {
	name string         // Service base name
	on [NumLevels]bool  // Indicators of whether the service is enabled
			    // on each runlevel
}

// The listing structure
type Listing struct {
	rows []ListingRow
}

type NameSelector func (string) bool
type NameChecker func () bool

// Create a Listing object for services from initdir that satisfy sel.
// Returns a pointer to the created object and error status.
func LoadListing(initdir *InitDir, sel NameSelector) (*Listing, error) {
	var lst Listing
	next := initdir.Iterator(sel)
	for srv := next(); srv != nil; srv = next() {
		lst.rows = append(lst.rows, ListingRow{name: srv.BaseName()})
	}
	return &lst, nil
}

// Mark the service name in the listing lst as enabled for runlevel lev.
func (lst *Listing) Set(name string, lev Runlevel) {
	for i := range lst.rows {
		if lst.rows[i].name == name {
			lst.rows[i].on[lev.Index()] = true
			return
		}
	}
}

// slackserv --list command
func List(args *ArgList) {
	// Load the content of the init.d directory.
	initdir, err := LoadInitDir(InitDirectory)
	if err != nil {
		log.Fatal(err)
	}

	// Runlevels required in command line (by the --level option, if any).
	rs := GetRunlevels()

	// Create a blank listing structure.
	lst, err := LoadListing(initdir, args.GetNameSelector())
	if err != nil {
		log.Fatal(err)
	}
	// Check and report unconsumed arguments
	args.Report()

	// Iterate over selected runlevels
	next := rs.Iterator()
	for r := next(); r.IsValid(); r = next() {
		// Scan the rc<r>.d directory.
		rcd, err := LoadRCDir(r, initdir)
		if err != nil {
			continue
		}
		// Iterate over all SystemV services in its start sequence.
		nextserv := rcd.Start.SysVServiceIterator()
		for srv := nextserv(); srv != nil; srv = nextserv() {
			// Mark each such service as enabled for the runlevel.
			lst.Set(srv.BaseName(), r)
		}
	}

	// Compute the width of the service name column
	width := 16
	if opts.Wide {
		for _, row := range lst.rows {
			w := len(row.name)
			if w > width {
				width = w
			}
		}
	}

	// Print the resulting listing.
	for _, row := range lst.rows {
		name := row.name
		nlen := len(name)
		if nlen > width {
			name = name[0:width-5] + `...` + name[nlen-2:]
		}

		fmt.Printf("%-*s", width, name)
		next = rs.Iterator()
		for r := next(); r.IsValid(); r = next() {
			var s string
			if (row.on[r.Index()]) {
				s = "on"
			} else {
				s = "off"
			}
			fmt.Printf("   %s:%-3s", r.Letter(), s)
		}
		fmt.Println()
	}
}

// -------------------------------------
// 2. Delete services
// -------------------------------------

func Del(args *ArgList) {
	if args.Len() == 0 {
		log.Fatal(`not enough arguments`)
	}

	// Load the content of the init.d directory.
	initdir, err := LoadInitDir(InitDirectory)
	if err != nil {
		log.Fatal(err)
	}

	// Get the required runlevels
	rs := GetRunlevels()

	// Collect the names of files to remove
	oplist := new(FileOpList)

	// Repeat for each service named in the command line:
	nextservice := initdir.Iterator(args.GetNameSelector())
	for wanted := nextservice(); wanted != nil; wanted = nextservice() {
		// iterate over all requested runlevels
		nextlevel := rs.Iterator()
		for r := nextlevel(); r.IsValid(); r = nextlevel() {
			// Scan the rc<r>.d directory.
			rcd, err := LoadRCDir(r, initdir)
			if err != nil {
				continue
			}

			// Iterate over all SystemV services in its start
			// sequence:
			nextsysv := rcd.Start.SysVServiceIterator()
			for srv := nextsysv(); srv != nil; srv = nextsysv() {
				// If the service basename matches, add its
				// file name to the list.
				if srv.BaseName() == wanted.BaseName() {
					oplist.Append(NewUnlink(srv.FileName()))
				}
			}

			// Do the same for all services in the stop
			// sequence.
			nextsysv = rcd.Stop.SysVServiceIterator()
			for srv := nextsysv(); srv != nil; srv = nextsysv() {
				if srv.BaseName() == wanted.BaseName() {
					oplist.Append(NewUnlink(srv.FileName()))
				}
			}
		}
	}
	// Check and report any unconsumed arguments
	args.Report()

	// Actually remove the collected files (unless in dry-run mode).
	err = oplist.Commit()
	if err != nil {
		log.Println(err)
	}
}

// -------------------------------------
// 3. Add services
// -------------------------------------

// Return a sequence of services from srvin and all services they depend
// on, ordered so that any given service precedes the services that depend
// upon it.
func (d *InitDir) AddDependencies(srvin []Service) (srvout []Service) {
	srvmap := make(map[string]int)
	for i := 0; i < len(srvin); {
		srv := srvin[i]
		if _, found := srvmap[srv.BaseName()]; found {
			srvin = append(srvin[0:i], srvin[i+1:]...)
			continue
		} else {
			srvmap[srv.BaseName()] = i
			i++
		}
		for _, sym := range srv.RequiredStart() {
			if IsBuiltinServiceName(sym) {
				continue //FIXME
			}
			dep := d.WhoProvides(sym)
			if (len(dep) > 0) {
				// FIXME: Selecting first one. Better ideas?
				srvin = append(srvin, dep[0])
			} else {
				log.Fatal(`unresolved symbol: `+sym)
			}
		}
	}

	// Build dependency matrix
	dm := NewDepmap(len(srvin))
	for i, srv := range srvin {
		for _, sym := range srv.RequiredStart() {
			dm.Set(i, srvmap[d.WhoProvides(sym)[0].BaseName()])
		}
	}

	// Compute transitive closure and shortest paths.  Report eventual
	// cyclic dependencies.
	p := dm.FindCycles()
	reportCyclicDependencies(p, srvin)

	// Keep track of already processed services
	srvind := make([]bool, dm.Dim())

	// Build the output array
	for i := 0; i < dm.Dim(); i++ {
		for j := 0; j < dm.Dim(); j++ {
			if dm.IsSet(i, j) {
				path := dm.Path(i, j)
				for k := len(path) - 1; k > 0; k-- {
					n := path[k]
					if !srvind[n] {
						srvout = append(srvout,
								srvin[n])
						srvind[n] = true
					}
				}
			}
		}
		if !srvind[i] {
			srvout = append(srvout, srvin[i])
			srvind[i] = true
		}
	}

	return
}

func Add(args *ArgList) {
	// Load the content of the init.d directory.
	initdir, err := LoadInitDir(InitDirectory)
	if err != nil {
		log.Fatal(err)
	}

	// Get the required runlevels
	rsmask := GetRunlevels()

	var services []Service // Services to be added
	var rsStart Runlevels  // Start levels
	var rsStop Runlevels   // Stop levels
	var rcdir [NumLevels]*RCDir  // All needed rcN.d directories

	// Collect services to be added
	if args.Len() > 0 {
		nextservice := initdir.Iterator(args.GetNameSelector())
		for srv := nextservice(); srv != nil; srv = nextservice() {
			services = append(services, srv)
		}
		// If any unused arguments remain, report them and exit.
		args.Report()
	} else if opts.Defaults {
		s := make(map[string]bool)
		nextlevel := GetRunlevels().Iterator()
		for r := nextlevel(); r.IsValid(); r = nextlevel() {
			rcd, err := LoadRCDir(r, initdir)
			if err != nil {
				log.Fatal("failed to load rc directory: " + err.Error())
			}
			rcdir[r.Index()] = rcd

			nextserv := rcd.Start.SysVServiceIterator()
			for srv := nextserv(); srv != nil; srv = nextserv() {
				if ! s[srv.BaseName()] {
					services = append(services, srv)
					s[srv.BaseName()] = true
				}
			}

			nextserv = rcd.Stop.SysVServiceIterator()
			for srv := nextserv(); srv != nil; srv = nextserv() {
				if ! s[srv.BaseName()] {
					services = append(services, srv)
					s[srv.BaseName()] = true
				}
			}
		}
	} else {
		log.Fatal(`not enough arguments`)
	}


	if opts.Recursive {
		services = initdir.AddDependencies(services)
	}

	// Compute the set of runlevels affected
	for _, srv := range services {
		rsStart.Or(srv.DefaultStart())
		rsStop.Or(srv.DefaultStop())
	}

	// Mask out non requested runlevels
	rsStart.And(rsmask)
	rsStop.And(rsmask)

	// Load rc directories
	var rs Runlevels             // A union of start and stop runlevels
	rs.Or(rsStart)
	rs.Or(rsStop)
	// Iterate over all runlevels and load the corresponding rc directories
	nextlevel := rs.Iterator()
	for r := nextlevel(); r.IsValid(); r = nextlevel() {
		if rcdir[r.Index()] == nil {
			rcd, err := LoadRCDir(r, initdir)
			if err != nil {
				log.Fatal("failed to load rc directory: " + err.Error())
			}
			rcdir[r.Index()] = rcd
		}
	}

	// Array of symlinks to be created
	oplist := new(FileOpList)

	// Incorporate each new service into the corresponding runlevels
	for _, srv := range services {
		if opts.Debug {
			log.Printf("DEBUG: adding %s\n", srv.BaseName())
		}

		nextlevel := rsStart.Iterator()
		for r := nextlevel(); r.IsValid(); r = nextlevel() {
			if s := rcdir[r.Index()].Start.Inc(srv); s != nil {
				oplist.Concat(s)
			}
		}

		nextlevel = rsStop.Iterator()
		for r := nextlevel(); r.IsValid(); r = nextlevel() {
			if s := rcdir[r.Index()].Stop.Inc(srv); s != nil {
				oplist.Concat(s)
			}
		}
	}

	// Create the symlinks
	err = oplist.Commit()
	if err != nil {
		log.Fatal(err)
	}
}

type Priority interface {
	Incr()
	Update(int)
	Value() int
	String() string
}

type PriorityBase struct {
	val int
}

type StartPriority PriorityBase
type StopPriority PriorityBase

func NewPriority(isstart bool) Priority {
	if isstart {
		return &StartPriority{0}
	} else {
		return &StopPriority{99}
	}
}

// Start priority implementation
// -----------------------------
func (p *StartPriority) Incr() {
	p.val++
	if p.val > 99 {
		p.val = 99
	}
}

func (p *StartPriority) Update(n int) {
//	log.Printf("Updating start priority %d %d\n", p.val, n)
	if p.val < n {
		p.val = n
	}
}

// Stop priority implementation
// -----------------------------
func (p *StopPriority) Incr() {
	p.val--
	if p.val < 0 {
		p.val = 0
	}
}

func (p *StopPriority) Update(n int) {
//	log.Printf("Updating stop priority %d %d\n", p.val, n)
	if p.val > n {
		p.val = n
	}
}

func (p *StartPriority) Value() int {
	return p.val
}

func (p *StartPriority) String() string {
	return fmt.Sprintf("%02d", p.Value())
}

func (p *StopPriority) Value() int {
	return p.val
}

func (p *StopPriority) String() string {
	return fmt.Sprintf("%02d", p.Value())
}

// Return an iterator over all dependencies of the service n in the
// start sequence.
func (rseq *RCSeq) StartTraversal(n int) ServiceIterator {
	next, err := rseq.Deps.TraverseRow(n)
	if err != nil {
		log.Fatal(err)
	}

	return func () Service {
		if i, more := next(); more {
			return rseq.Service[i]
		}
		return nil
	}
}

// Return an iterator over all services with the same dependencies as
// service n.
func (rseq *RCSeq) TraverseEquidependentServices(n int) ServiceIterator {
	next, err := rseq.Deps.TraverseMatchingRows(n)
	if err != nil {
		log.Fatal(err)
	}
	next() // Skip row 0 ($undefined)
	return func () Service {
		if i, more := next(); more {
			return rseq.Service[i]
		}
		return nil
	}
}

func reportCyclicDependencies(p [][]int, srv []Service) {
	if len(p) > 0 {
		log.Printf("Cyclic dependencies detected:\n")
		for _, t := range p {
			s := make([]string, len(t))
			for i, n := range t {
				s[i] = srv[n].BaseName()
			}
			log.Printf("%s\n", strings.Join(s, ` -> `))
		}
		os.Exit(1)
	}
}

// Detect and report possible cyclic dependencies.
func (rseq *RCSeq) DetectRecursion() {
	reportCyclicDependencies(rseq.Deps.Copy().FindCycles(),
				 rseq.Service)
}

// Incorporate service newsrv into sequence rseq.
func (rseq *RCSeq) Inc(newsrv Service) *FileOpList {
	oplist := new(FileOpList)

	oldsrv := rseq.FindServiceByName(newsrv.BaseName())
	if oldsrv != nil {
		// If link already exists and --defaults is requested,
		// unlink it first.
		if opts.Defaults {
			oplist.Append(NewUnlink(oldsrv.FileName()))
			rseq.DeregisterService(oldsrv)
		} else {
			return nil
		}
	}

	/* 1. Register newserv in rsec. Let N be its number.
	   2. Compute dependency matrix
	   3. Compute priority for the new service:
	   3.1. Set prio to initial value (0 for start sequence, 99 for
		stop sequence)
	   3.2. Traverse the Nth row of the matrix.
		 For each element (i,j) that is true, set

		    prio = update(prio, srv[j].prio)

		 where update is max for start sequence and min for stop.
	   4. Create symlink name and return new Symlink structure.
	*/
	srvno := rseq.RegisterService(NewRCService(newsrv))
	rseq.BuildDependencies()
	if rseq.Isstart && rseq.Deps.IsSet(srvno, 0) {
		log.Printf("unresolved dependencies")
		return nil
	}
	rseq.DetectRecursion()

	prio := NewPriority(rseq.Isstart)
	next := rseq.StartTraversal(srvno)
	for srv := next(); srv != nil; srv = next() {
		if ok, diag := srv.Enabled(); !ok {
			log.Printf("%s: %s depends on %s, which is not enabled\n",
					rseq.dir.runlevel.Letter(),
					rseq.Service[srvno].BaseName(),
					srv.BaseName());
			log.Printf("%s\n", diag);
			return nil
		}
		prio.Update(srv.Priority())
	}
	prio.Incr()
	// Traverse services that have the same dependencies
	next = rseq.TraverseEquidependentServices(srvno)
	for srv := next(); srv != nil; srv = next() {
		prio.Update(srv.Priority())
	}
	rseq.Service[srvno].SetPriority(prio.Value())

	var initial string
	if rseq.Isstart {
		initial = `S`
	} else {
		initial = `K`
	}
	oplist.Append(&Symlink{src: filepath.Join(InitDirectory,
					   rseq.Service[srvno].BaseName()),
			       dst: filepath.Join(rseq.dir.dirname,
				       initial + prio.String() + rseq.Service[srvno].BaseName())})
	return oplist
}
