package main

import (
	"os"
	"bufio"
	"regexp"
	"strconv"
	"errors"
	"unicode"
	"log"
)

var (
	initInfoRe = regexp.MustCompile(`^#\s+([A-Za-z_][A-Za-z0-9_-]*):\s*(.*)`)
	contRe = regexp.MustCompile(`^#\s+(.*)$`)
	delimRe = regexp.MustCompile(`\s+`)
	beginInitInfoRe = regexp.MustCompile(`^###\s+BEGIN\s+INIT\s+INFO\s*$`)
	endInitInfoRe = regexp.MustCompile(`^###\s+END\s+INIT\s+INFO\s*$`)
)

// SysVService
type SysVService struct {
	ServiceBase
	requiredStart []string
	requiredStop []string
	shouldStart []string
	shouldStop []string
	defaultStart Runlevels
	defaultStop Runlevels
}

func (srv *SysVService) Copy(src Service) {
	srv.ServiceBase.Copy(src)
	srv.provided      = src.Provided();
	srv.requiredStart = src.RequiredStart();
	srv.requiredStop  = src.RequiredStop();
	srv.shouldStart   = src.ShouldStart();
	srv.shouldStop    = src.ShouldStop();
	srv.defaultStart  = src.DefaultStart();
	srv.defaultStop   = src.DefaultStop();
	srv.shortDescr    = src.ShortDescr();
	srv.descr         = src.Descr();
}

func (srv *SysVService)	RequiredStart() []string {
	return srv.requiredStart[:]
}
func (srv *SysVService)	RequiredStop() []string {
	return srv.requiredStop[:]
}
func (srv *SysVService) ShouldStart() []string {
	return srv.shouldStart[:]
}
func (srv *SysVService)	ShouldStop() []string {
	return srv.shouldStop[:]
}
func (srv *SysVService)	DefaultStart() Runlevels {
	return srv.defaultStart
}
func (srv *SysVService)	DefaultStop() Runlevels {
	return srv.defaultStop
}

// VAL is a string containing space-separated runlevel letters.
// KW is the name of the keyword in the service file to which VAL
// pertains.  It is used for error reporting only.
// The function converts VAL to a Runlevels object and returns it
// along with the error status (nil if no error, or an instance of
// ServiceVariableError otherwise).
func ToRunlevels(kw string, val string) (Runlevels, error) {
	var rl Runlevels
	for _, c := range val {
		if unicode.IsSpace(c) {
			continue
		}
		if err := rl.Set(string(c)); err != nil {
			return rl, &ServiceVariableError{name: kw,
					     msg: err.Error(),
					     fatal: true}
		}
	}
	return rl, nil
}

// A service variable error
type ServiceVariableError struct {
	name string          // Keyword that caused the error.
	msg string           // Error message.
	fatal bool           // True if the error is fatal.
}

func (e ServiceVariableError) Error() string {
	return e.name + ": " + e.msg
}

func (e ServiceVariableError) VarName() string {
	return e.name
}

func (e ServiceVariableError) IsFatal() bool {
	return e.fatal
}

func splitLine(val string) []string {
	a := delimRe.Split(val, -1)
	l := len(a)
	if a[l-1] == `` {
		a = a[:l-1]
	}
	return a
}

// Assign the value VAL to the variable KW in the system-V service A.
// If CONT is true, it is a continuation string (valid only for
// Descripion).
// Returns nil on success, and an instance of ServiceVariableError on error.
func (s *SysVService) Assign (kw string, val string, cont bool) error {
	var err error

	if matched, _ := regexp.MatchString(`^X-`, kw); matched {
		return nil
	}
	if cont && kw != `Description` {
		return &ServiceVariableError{name: kw,
					     msg: "variable doesn't allow continuation",
					     fatal: true}
	}
	switch kw {
	case `Provides`:
		if len(s.provided) != 0 {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		s.provided = splitLine(val)
	case `Require-Start`,`Required-Start`:
		if len(s.requiredStart) != 0 {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		if val != `` {
			s.requiredStart = splitLine(val)
		}
	case `Require-Stop`,`Required-Stop`:
		if len(s.requiredStop) != 0 {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		if val != `` {
			s.requiredStop = splitLine(val)
		}
	case `Should-Start`:
		if len(s.shouldStart) != 0 {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		if val != `` {
			s.shouldStart = splitLine(val)
		}
	case `Should-Stop`:
		if len(s.shouldStop) != 0 {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		if val != `` {
			s.shouldStop = splitLine(val)
		}
	case `Default-Start`:
		// FIXME
		// if s.DefaultStart != nil {
		//	return &ServiceVariableError{name: kw,
		//				     msg: "variable redefined",
		//				     fatal: true}
		// }
		if val != `` {
			s.defaultStart, err = ToRunlevels(kw, val);
		}
	case `Default-Stop`:
		// FIXME
		// if s.DefaultStop != nil {
		//	return &ServiceVariableError{name: kw,
		//				     msg: "variable redefined",
		//				     fatal: true}
		// }
		if val != `` {
			s.defaultStop, err = ToRunlevels(kw, val)
		}
	case `Short-Description`:
		if s.shortDescr != `` {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		}
		if val != `` {
			s.shortDescr = val
		}
	case `Description`:
		if (cont) {
			s.descr += "\n" + val
		} else if s.descr != `` {
			return &ServiceVariableError{name: kw,
						     msg: "variable redefined",
						     fatal: true}
		} else {
			s.descr = val
		}
	default:
		err = &ServiceVariableError{name: kw,
					    msg:  "unrecognized variable",
					    fatal: false}
	}
	return err
}

// Report error ERR in file F, line L.
func FileErrorLog(f string, l int, err error) {
	log.Println(f + ":" + strconv.Itoa(l) + ": " + err.Error())
}

// Same as above, but for warnings.
func FileWarningLog(f string, l int, err error) {
	log.Println(f + ":" + strconv.Itoa(l) + ": warning: " + err.Error())
}

// Parse the LSB headers from file F and return the corresponding Service
// structure and error object (if any).
func ReadServiceFile(f string) (Service, error) {
	file, err := os.Open(f)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	line := 0

	const (
		stateStart = iota
		stateInit
		stateStop
		stateErr
	)
	state := stateStart
	var service SysVService
	service.SetFileName(f)
	var last_keyword string
	for state < stateStop && scanner.Scan() {
		line++
		s := scanner.Text()
		switch state {
		case stateStart:
			if beginInitInfoRe.MatchString(s) {
				state = stateInit
			}
		case stateInit:
			if endInitInfoRe.MatchString(s) {
				state = stateStop
			} else if res := initInfoRe.FindStringSubmatch(s); res != nil {
				err = service.Assign(res[1], res[2], false)
				if (err == nil) {
					last_keyword = res[1]
				} else if ve, ok := err.(*ServiceVariableError); ok && ve.IsFatal() {
					FileErrorLog(f, line, err)
					state = stateErr
				} else {
					FileWarningLog(f, line, err)
					// FIXME
					break
				}
			} else if res := contRe.FindStringSubmatch(s); res != nil {
				if err := service.Assign(last_keyword, res[1], true); err != nil {
					FileErrorLog(f, line, err)
					state = stateErr
				}
			} else {
				state = stateStop
				break
			}
		}
	}

	switch state {
	case stateInit:
		return nil, errors.New(f + ": no init info section")
	case stateErr:
		return nil, errors.New(f + ": errors in init info section")
	default:
		// if len(service.provided) == 0 || (len(service.DefaultStart) == 0 && len(service.DefaultStop) == 0) {
		//	   return nil, errors.New(f + ": incomplete init info section")
		// }
	}
	return &service, nil
}
