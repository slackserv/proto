package main

import (
	"os"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strconv"
	"reflect"
	"log"
)

// A (start-up or shut-down) sequence of services.
type RCSeq struct {
	Service []Service         // Array of services in the sequence
	Deps *Depmap              // A matrix of dependencies between services
	Symbols map[string]int    // A map of symbols provided by each service.
				  // The value of Symbols[name] is the index
				  // of the corresponding service entry in the
				  // Service array.
	Isstart bool              // True if this is a start-up sequence.
	dir *RCDir                // Back pointer to the directory structure.
}

// Representation of a rcN.d directory
type RCDir struct {
	runlevel Runlevel  // Corresponding runlevel (N)
	dirname string     // Full name of the directory
	Start RCSeq        // Start-up sequence (S)
	Stop RCSeq         // Shut-down sequence (K)
}

type RCIface interface {
	RegisterService(Service) int
	BuildDependencies()
	ProvidesSymbol(string) bool
}

// Registers service SRV in SEQ.  Returns integer descriptor of that service.
func (seq *RCSeq) RegisterService(srv Service) (n int) {
	n = len(seq.Service)
	for _, s := range srv.Provided() {
		if _, found := seq.Symbols[s]; !found {
			seq.Symbols[s] = n
		}
	}
	seq.Service = append(seq.Service, srv)
	return
}

func (seq *RCSeq) FindServiceIndex(name string) int {
	for i, s := range seq.Service {
		if name == s.BaseName() {
			return i
		}
	}
	return -1
}

func (seq *RCSeq) DeregisterService(srv Service) {
	n := seq.FindServiceIndex(srv.BaseName())
	if (n == -1) {
		return
	}
	for _, s := range srv.Provided() {
		if i, found := seq.Symbols[s]; found && i == n {
			delete(seq.Symbols, s)
		}
	}
	seq.Service = append(seq.Service[0:n], seq.Service[n+1:]...)
}

// Registers all built-in services in the sequence SEQ.
// FIXME: Not all built-in services are present in all runlevels.
func (seq *RCSeq) RegisterBuiltinServices() {
	next := BuiltinServiceIterator(seq.Isstart)
	for srv := next(); srv != nil; srv = next() {
		seq.RegisterService(srv)
	}
}

// Build the dependency matrix for services in sequence SEQ.
func (seq *RCSeq) BuildDependencies() {
	n := len(seq.Service)
	seq.Deps = NewDepmap(n)
	for i, dependency := range seq.Service {
		var required []string
		if seq.Isstart {
			required = dependency.RequiredStart()
		} else {
			required = dependency.RequiredStop()
		}

		for _, sym := range required {
			found := false
			for j, pretender := range seq.Service {
				if pretender.Provides(sym) {
					seq.Deps.Set(i, j)
					found = true
					break
				}
			}
			if ! found && seq.Isstart {
				log.Printf("%s: dependency %s not found in runlevel %s\n",
					      dependency.BaseName(),
					      sym,
					      seq.dir.runlevel.Letter())
				seq.Deps.Set(i, 0)
			}
		}
	}
}

// Return an interator over all SystemV services in SEQ.
func (seq *RCSeq) SysVServiceIterator() ServiceIterator {
	i := 1 // Skip the initial $undefined
	return func () Service {
		for i < len(seq.Service) {
			srv := seq.Service[i]
			i++

			if reflect.TypeOf(srv) == reflect.TypeOf(&RCService{}) {
				return srv
			}
		}
		return nil
	}
}

func (seq *RCSeq) FindServiceByName(name string) Service {
	for _, srv := range seq.Service {
		if srv.BaseName() == name {
			return NewRCService(srv)
		}
	}
	return nil
}

// Create a RC sequence corresponding to the directory rcd.
// RSEQ must be either rcd.Start or rcd.Stop.
func (rseq *RCSeq) InitRCSeq(rcd *RCDir) {
	rseq.Symbols = make(map[string]int)
	rseq.Isstart = rseq == &rcd.Start
	rseq.dir = rcd

	srv := ServiceBase{baseName: `$undefined`,
			   shortDescr: `Undefined service`}
	rseq.Service = append(rseq.Service, NewRCService(&srv))

	rseq.RegisterBuiltinServices()
}

var scriptName = regexp.MustCompile(`^([KS])([0-9][0-9])(.*)`)

// S being a file name, follow all symbolic links and return the
// corresponding absolute file name.
func realpath(s string) (string, error) {
	path, err := filepath.EvalSymlinks(s)
	if err != nil {
		return s, err;
	}
	return filepath.Abs(path);
}

// Scan the rc-directory for the runlevel 'level'. 'idir' provides the
// parsed-out init.d directory.
func LoadRCDir(level Runlevel, idir *InitDir) (*RCDir, error) {
	dirname := filepath.Join(opts.RCDirectory, `rc` + level.Letter() + `.d`)

	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	rl := RCDir{dirname: dirname, runlevel: level}
	rl.Start.InitRCSeq(&rl)
	rl.Stop.InitRCSeq(&rl)

	for _, file := range files {
		match := scriptName.FindStringSubmatch(file.Name())
		if match == nil ||
			!(file.Mode().IsRegular() ||
			  (file.Mode() & os.ModeSymlink) != 0) {
			continue
		}

		path, err := realpath(filepath.Join(dirname, file.Name()))
		if err != nil {
			log.Println(path + ": " + err.Error())
			continue
		}

		st, err := os.Stat(path)
		if err != nil {
			log.Printf("%s: can't stat: %s\n", path, err.Error())
			continue
		}

		srv, found := idir.FindServiceByFile(st)
		if !found {
			log.Printf("%s: ignoring; %s target not found in %s\n", path, file.Name(), InitDirectory)
			continue
		}
		srv.SetFileName(filepath.Join(dirname, file.Name()))
		prio, _ := strconv.Atoi(match[2])
		srv.SetPriority(prio)
		if match[1] == `S` {
			rl.Start.RegisterService(srv)
		} else {
			rl.Stop.RegisterService(srv)
		}
	}
	rl.Start.BuildDependencies()
	rl.Stop.BuildDependencies()
	return &rl, nil
}
