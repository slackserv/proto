package main

import (
	"os"
	"log"
	"io/ioutil"
	"path/filepath"
	"syscall"
	"regexp"
)

// A representation of the init.d directory. Basically, an array of
// Service structure corresponding to the files it contains.
type InitDir struct {
	service []Service
}

// Ignore files matching this regexp.
var ignoreFileRe = regexp.MustCompile(`^(functions|#.*#|(.*(~|\.bak)))$`)

// Scan the init.d directory and return a pointer to its internal
// representation.
func LoadInitDir(dirname string) (*InitDir, error) {
	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		return nil, err
	}
	var dir InitDir

	for _, file := range files {
		if !ignoreFileRe.MatchString(file.Name()) &&
			file.Mode().IsRegular() &&
			(file.Mode() & syscall.S_IXUSR) != 0 {
			path := filepath.Join(dirname, file.Name())
			if srv, err := ReadServiceFile(path); err == nil {
				srv.SetBaseName(file.Name())
				srv.SetFileInfo(file)
				dir.service = append(dir.service, srv)
			} else {
				log.Println(path + ": " + err.Error())
			}
		}
	}
	return &dir, nil
}

// Return an iterator over services in init.d which statisfy the selector
// SEL.
func (d *InitDir) Iterator(sel NameSelector) ServiceIterator {
	i := 0
	return func() Service {
		for i < len(d.service) {
			srv := d.service[i]
			i++
			if sel(srv.BaseName()) {
				return srv
			}
		}
		return nil
	}
}

func (d *InitDir) WhoProvides(sym string) (result []Service) {
	for _, srv := range d.service {
		if (srv.Provides(sym)) {
			result = append(result, NewRCService(srv))
		}
	}
	return
}

// Given service base name, return a copy of the corresponding Service
// structure.
func (d *InitDir) FindServiceByName(name string) (Service, bool) {
	for _, srv := range d.service {
		if srv.BaseName() == name {
			return NewRCService(srv), true
		}
	}
	return nil, false
}

// Given service file info, return a copy of the corresponding Service
// structure.
func (d *InitDir) FindServiceByFile(info os.FileInfo) (Service, bool) {
	for _, srv := range d.service {
		if srv.SameFile(info) {
			return NewRCService(srv), true
		}
	}
	return nil, false
}
